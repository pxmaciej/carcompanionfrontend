import { HttpClient, HttpResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { shareReplay,map, tap } from 'rxjs/operators';

interface postCar{
  brand: string;
  model: string;
  generation: string;
  plate: string;
  mileage: number;
  productionYear: number;
}

interface getCars{
  userId:string;
  carId: string;
  mainName: string;
  brand: string;
  model: string;
  generation: string;
  plate: string;
  mileage:number;
  productionYear: number;
  users:[]
}

interface Expense{
  amount: number;
  description: string;
  mileageInterval: number;
  date: string;
  endOfDateInterval: string;
  category: string;
}

interface Share{
  success: boolean;
  shareKey: string;
}



@Injectable()
export class DashboardService {
  allexpenses = new BehaviorSubject<any>(null);
  allcategory = new BehaviorSubject<any>(null);
  allcars = new BehaviorSubject<any>(null);
  carid = new BehaviorSubject<any>(null);
  shareKey = new BehaviorSubject<any>(null);
  url='http://127.0.0.1:8080/api/v1/cars/';

  shareCar(fieldCar, id){
    this.http.post(this.url+id+'/share',fieldCar).subscribe((response:Share) =>{
      this.shareKey.next(response.shareKey)
    })
  }

  getCarById(id){
    this.carid.next(id)
    this.router.navigate(['/carEdit'])
  }

  postCar(car:postCar){
     this.http.post<postCar>('http://127.0.0.1:8080/api/v1/cars',car).subscribe(response =>{
      console.log(response);
      })
  }

  getCars(){
    this.http.get<getCars>('http://127.0.0.1:8080/api/v1/cars').subscribe(response =>{
      this.allcars.next(response);
    })
  }

  updateCar(car:postCar,id){
    this.http.patch<postCar>(this.url+id, car).subscribe(response=>{
      console.log(response);
    })
    this.router.navigate(['/dashboard']);
  }

  addExpense(expense:Expense, id){
    this.http.post<Expense>(this.url+id+'/expenses',expense).subscribe(response=>{
      console.log(response);
    })
  }

  getExpense(id){
    this.http.get(this.url+id+'/expenses').subscribe(response =>{
      this.allexpenses.next(response);
    })
  }

  getCategory(){
    this.http.get('http://127.0.0.1:8080/api/v1/cars/expenses/categories').subscribe(response =>{
      this.allcategory.next(response);
    })
  }

  deleteCar(id){
    this.http.delete(this.url+id).subscribe(response =>{
      console.log(response);
    })
    setTimeout(()=>{this.router.navigate(['/dashboard']),3000});
  }

  subscribeCare(key){
    this.http.post(this.url+'use-sharekey/'+key, null).subscribe(response =>{
      console.log(response);
    });
  }

  constructor(private http:HttpClient, private router: Router) { }


}
