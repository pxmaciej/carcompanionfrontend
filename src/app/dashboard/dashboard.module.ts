import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardService } from './dashboard.service';
import { EditCarComponent } from './edit-car/edit-car.component';



@NgModule({
  declarations: [DashboardComponent, EditCarComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [DashboardComponent, EditCarComponent],
  providers: [DashboardService]
})
export class DashboardModule { }
